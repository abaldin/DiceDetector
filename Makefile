# target name
TARGET	= DiceDetector
# compiler and linker settings
CC	= g++
CFLAGS	= -O3 -std=c++14 -Wall -fopenmp `pkg-config opencv --cflags`

LINKER	= g++ -o
LFLAGS	= -Wall -fopenmp -L/usr/local/lib

# OpenCV libs
LIBS	= `pkg-config opencv --cflags --libs`

# directories definition
SRCDIR	= src
INCDIR	= include
BINDIR	= bin

# wildcard expansions
SOURCES  := $(wildcard $(SRCDIR)/*.cpp)
INCLUDES := $(wildcard $(INCDIR)/*.h)
OBJECTS  := $(SOURCES:$(SRCDIR)/%.cpp=$(BINDIR)/%.o)

# rules
$(BINDIR)/$(TARGET): $(OBJECTS)
	@echo ">>> linking target "$@":"
	$(LINKER) $@ $(LFLAGS) $(OBJECTS) $(LIBS)
	@echo "Linking complete! <<<"

$(OBJECTS): $(BINDIR)/%.o : $(SRCDIR)/%.cpp
	@echo ">>> compiling "$<":"
	$(CC) $(CFLAGS) -Iinclude -c $< -o $@
	@echo ""$<" successfully compiled! <<<\n"

.PHONY: clean
clean:
	@echo ">>> cleaning up object files:"
	rm -f $(OBJECTS)
	@echo "Cleanup complete! <<<"

.PHONY: remove
remove: clean
	@echo ">>> removing executable:"
	rm -f $(BINDIR)/$(TARGET)
	@echo "Executable removed! <<<"

.PHONY: run
run: $(BINDIR)/$(TARGET)
	@echo ">>> starting executable <<<"
	./$(BINDIR)/$(TARGET)
