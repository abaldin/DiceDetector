/*
 * Dice.cpp
 *
 *  Created on: 14.10.2016
 *      Author: anton
 */
#include <cv.h>
#include <highgui.h>

#include <vector>
#include <map>
#include <algorithm>

#include "Timer.h"
#include "Dice.h"
#include "DiceTest.h"
#include "DiceUtil.h"

#include "CommandLine.h"

bool END_LOOP_REQUEST = false;
Timer globaltime;


int device_ID = 0;			// device id for VideoCapture
long img_size = 1500000;	// desired resolution of images to process
int ref_iter = 2;			// max number of reflection hole filling iterations

int minsize = 90;			// min and max blob sizes
int maxsize = 1000;


void startVideoCapture() {
	END_LOOP_REQUEST = false;
	
	cv::namedWindow("Video", CV_GUI_NORMAL);
	cv::VideoCapture cap = cv::VideoCapture(device_ID);
	//while(!cap.isOpened()) {
	//	std::cout << i <<" capture not opened!\n";
	//	cap.release();
	//	cap = cv::VideoCapture(++i);
	//}
	cv::Mat frame;
	while (!END_LOOP_REQUEST) {
		cap >> frame;
		pollInput(frame);
		cv::imshow("Video", frame);
	}
	cv::destroyWindow("Video");
	cap.release();

}


void pollInput(cv::Mat& currentFrame) {
	char keyPressed = cv::waitKey(1);

	switch(keyPressed) {
	case 27: //ESC
		END_LOOP_REQUEST = true;
		std::cout << "end of demo!" << std::endl;
		break;

	case'd':
		countDice(currentFrame, true);
		break;

	default:
		break;
	}
}


// make a binary mask denoting the approx. dice position
// uses circles or rectangles
void getDiceMask(cv::Mat &input, cv::Mat& output, int a, int b, bool extraBlur) {
    cv::cvtColor(input, output, CV_BGR2GRAY);
    cv::medianBlur(output, output, 5);
    cv::Canny(output, output, a, b, 3);
    Util::drawBoundingBoxes(output, output);
	if(extraBlur) cv::medianBlur(output, output, 11);

	quickShow("Dice Mask", output);
}

void getBackground(cv::Mat& input, cv::Mat& background, cv::Mat& mask) {
	cv::Mat blackImage = cv::Mat::zeros(input.size(), CV_8UC3);
	background = cv::Mat::zeros(input.size(), CV_8UC3);

	//creates an image with "black holes" where the dice lie
	cv::add(blackImage, input, background, 255-mask);

	//fills the black holes
	Util::patchBackground(background, 64, 149);
	//patchBackground_test(output, 64, 149);
	quickShow("Background", background);
}

void sumUpBgSubtractions(cv::Mat& image, cv::Mat& background, int thr_first, int thr_last, std::vector<cv::Mat>& results) {
	Timer t; t.start();
	cv::Mat result(image.size(), CV_8UC3, cv::Scalar(0,0,0));
	for(int threshold = thr_first; threshold >= thr_last; threshold-=4) {
			cv::Mat cp = image.clone();
			Util::subtractBackground(cp, background, threshold);
			cv::medianBlur(cp, cp, 3); //prevent dots sticking together by noise between them
			//cv::blur(cp, cp, cv::Size(3,3));
			result += cp;
			results.push_back(result.clone());
			//push back an inverted version of the image to enable white dot detection
			results.push_back(cv::Scalar(255,255,255)-result.clone());
	}
	std::cout << globaltime << t << " sumUpBgSubtractions: " << results.size() << " images created.\n";
}

//input image should be thresholded!
//TODO: distinguish between black and white dots!
void getRidOfReflections(cv::Mat& image, cv::Mat& mask, int maxIterations, bool do_threshold) {
	if(maxIterations == 0) return;
	Timer t; t.start();
	cv::Vec3b dotColor = cv::Vec3b(0,0,0);
	if(do_threshold) {
		cv::cvtColor(image, image, CV_BGR2GRAY);
    	cv::threshold(image, image,  100, 255, CV_THRESH_BINARY);
    	cv::cvtColor(image, image, CV_GRAY2BGR);
    	//quickShow("Reflections threshold", image);
	}

	//cv::Mat lastIteration;
	int counter=0;

	do{ //stop when hole filling does not change the image anymore (bad idea because it takes too long)
		//lastIteration = image.clone();
		Util::fillHolesInCircles(image, mask, dotColor);
		cv::transpose(image, image); cv::transpose(mask, mask);
		Util::fillHolesInCircles(image, mask, dotColor);
		cv::transpose(image, image); cv::transpose(mask, mask);
		counter++;
	} while (counter < maxIterations /*&& (cv::sum(image != lastIteration) != cv::Scalar(0,0,0))*/);
	std::cout << globaltime << t;
	std::cout << " Hole filling finished after " << counter << " iterations.\n";
}


// experimental: not used yet
// idea: define each dice as a roi to compromise the missing mask support for blob detection
void getROIs(cv::Mat mask, std::vector<cv::Rect>& ROIs) {
	std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::Vec4i> hierarchy;
    /// Find contours
    cv::findContours(mask, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));

    /// Approximate contours to polygons + get bounding rects and circles
    std::vector<std::vector<cv::Point> > contours_poly(contours.size());
    //vector<Rect> boundRect(contours.size());

    for (int i = 0; i < contours.size(); i++) {
        cv::approxPolyDP(cv::Mat(contours[i]), contours_poly[i], 3, true);
        ROIs.push_back(cv::boundingRect(cv::Mat(contours_poly[i])));
    }
}


int blobDetect(cv::Mat &image, cv::Mat& mask, bool override) {
	Timer t; t.start();

	//cv::Mat mask;
    // Setup SimpleBlobDetector parameters.
    cv::SimpleBlobDetector::Params params;

    // Change thresholds
    params.minThreshold = 10;
    params.maxThreshold = 100;

    // Filter by Area.
    params.filterByArea = true;
    params.minArea = minsize; //90
	params.maxArea = maxsize;
	//for BIG dots. but does not work well with small ones..
	//params.maxArea = 8000;

    // Filter by Circularity
    params.filterByCircularity = true;
    params.minCircularity = 0.80; //square: 0.785, pentagon: 0.865 hexagon: 0.907, octagon: 0.948

    // Filter by Convexity
    params.filterByConvexity = true;
    params.minConvexity = 0.7; //some dents still remain sometimes

    // Filter by Inertia
    params.filterByInertia = true;
    params.minInertiaRatio = 0.5;

    // Set up detector with params
    cv::SimpleBlobDetector detector(params);
	std::vector<cv::KeyPoint> keypoints;
    detector.detect(image, keypoints, mask);

    //cv::Mat im_with_keypoints(image.size(), CV_8UC3, cv::Scalar::all(0));
    cv::drawKeypoints(image, keypoints, image, cv::Scalar(0, 0, 255), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
	std::cout << globaltime << t <<" Blob detection: found " << keypoints.size() << " blobs.\n";
	return keypoints.size();
}


int countDice(cv::Mat image, bool writeToDisk) {
	if(img_size != 0) Util::resizeImage(image, img_size);
	globaltime.start();
	std::vector<cv::Mat> results; 		// image range of background subtraction
	std::map<cv::Mat*, int> dotCounts;	// map for individual dot counts
	cv::Mat background, mask;			// matrices for storing background and mask

	std::vector<cv::Rect> ROIs;
	
	getDiceMask(image, mask);
	getBackground(image, background, mask);
	
	//getROIs(mask.clone(), ROIs); buggy as f
	std::cout << "number of rois: " << ROIs.size() << std::endl;

	sumUpBgSubtractions(image, background, 100, 8, results);

	//Init map. If keys are already inserted, parallel access is possible. 
	for(cv::Mat& img : results) {
		img.setTo(cv::Scalar(0,0,0), 255-mask); //apply mask manually because the blobdetector crap is broken
		dotCounts[&img] = 0;
	}

	//mask gets transposed -> private!
	#pragma omp parallel for shared(results) firstprivate(mask)
	for(size_t i = 0; i<results.size(); i++) {
		getRidOfReflections(results[i], mask, ref_iter, true);
		int blob_count = blobDetect(results[i], mask, true);

		dotCounts[&(results[i])] = blob_count;
	}

	//determine max blob count and display corresponding image
	auto max_image = std::max_element(dotCounts.begin(), dotCounts.end(),
	[](const auto& p1, const auto& p2) { //auto in lambdas requeires C++ 14!
		return p1.second < p2.second;
	});

	//show best result
	std::cout << "number of dots: " << max_image->second << std::endl;
	quickShow("Best Image", *max_image->first);

	//write processed images to disk
	if(writeToDisk) {
		std::cout << "writing processed images to disk...\n"; int n=0;
		for(auto img : dotCounts) {
			if(img.second > 0) //don't write files without any results
				cv::imwrite("./results/"+std::to_string(n++)+" - result Is ["+std::to_string(img.second)+"].png", *img.first);
		}
		std::cout << "done!\n";
	}
	return max_image->second;
} //end of countDice.

int main( int argc, char** argv )
{
	//cv::Mat image = cv::imread("res/dice15.jpeg", CV_LOAD_IMAGE_COLOR);
	//if(!image.data) std::cerr << "Error loading image" << std::endl;

	//std::cout << loadTestImages() << " Images loaded!" << std::endl;
	int n;
	if(argc > 1) {			//test with local images
		n = atoi(argv[1]);
		std::cout << loadTestImages() << " Images loaded!" << std::endl;
		cv::Mat image = getTestImage(n).first;
		std::cout << "current image: " << n << std::endl;

		countDice(image);
		cv::waitKey(-1);
		imwrite("./output.bmp", image);
		return 0;

	}

	//init command line and show prompt:
	cmd::init();
	cmd::showPrompt();

}

