

#include <iostream>
#include <string>
#include <map>
#include <functional>

#include <thread>

#include "CommandLine.h"
#include "DiceTest.h"
#include "Dice.h"

#include <cv.h>
#include <highgui.h>

using namespace std;

bool exit_request = false;
map<string, function<void()> > commands;

// some values that can be set 
extern int device_ID;
extern long img_size;
extern int ref_iter;

extern int minsize, maxsize;

void cmd::addCommand(string cmd, function<void()> func) {
    commands[cmd] = func;
}

void cmd::init() {
    addCommand("exit", []() {
        exit_request = true;
        cout << "Good bye!" << endl;
        return;
    });

    addCommand("test", []() {
        int n;
        //cout << "image to test: ";
        cin >> n;
        if(n < 0) return;
        //keep windows active until user presses esc 
        //std::thread([n]() {
            auto img = getTestImage(n);
            quickShow("test image", img.first);
            if(countDice(img.first) == img.second) cout<< "SUCCESS!" << endl;
            else cout << "FAILED: correct result is " << img.second << endl;
            while(!cv::waitKey(0));
            cv::destroyAllWindows();
        //}).detach();

        
    });

    addCommand("testall", []() {
        runAutomatedTests();
    });

    addCommand("demo", []() {
        cout << "Demo started!\n";
        startVideoCapture();
        cv::destroyAllWindows();
    });

    addCommand("set", []() {
        string input;
        cin >> input;

        if(input == "list") {
            cout << "device\t" << device_ID << endl;
            cout << "imgsize\t" << img_size << endl;
            cout << "refiter\t" << ref_iter << endl;
            cout << "minsize\t" << minsize << endl;
            cout << "maxsize\t" << maxsize << endl;
        }

        else if(input == "device") {
            cin >> device_ID;
            cout << "device_ID set to " << device_ID << endl;
        }
        else if(input == "imgsize") {
            cin >> img_size;
            cout << "img_size set to " << img_size << endl;
        }
        else if(input == "refiter") {
            cin >> ref_iter;
            cout << "ref_iter set to " << ref_iter << endl;
        } else if(input == "minsize") {
            cin >> minsize;
            cout << "minsize set to " << minsize << endl;
        }
        else if(input == "maxsize") {
            cin >> maxsize;
            cout << "maxsize set to " << maxsize << endl;
        }

    });

    addCommand("help", []() {
        cout << "List of commands:\n";
        for(auto cmd : commands)
            cout << cmd.first << endl;
    });

}

void cmd::showPrompt() {
    while(!exit_request) {
        string input;
	    cout << ">> ";
        cin >> input;

        auto cmd = commands.find(input);
        if(cmd != commands.end())
            (cmd->second)();
    }

}







