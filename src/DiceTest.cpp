/*
 * DiceTest.cpp
 *
 * Interactive testing with the pipeline.
 *
 *  Created on: 26.11.2016
 *      Author: anton
 */

#include <cv.h>
#include <highgui.h>
#include <vector>
#include <map>

#include <fstream>

#include "Pipeline.h"
#include "DiceUtil.h"
#include "DiceTest.h"

#include "Dice.h"
#include "Timer.h"

using namespace std;
using namespace cv;

vector<pair<Mat, int> > testImages;


int loadTestImages() {
	ifstream dotsFile("test/dots");
	if(dotsFile.is_open()) {
		string line;
		while(getline(dotsFile, line)) {
			if(line.at(0) == '#') continue;	//comment
			size_t tabPos = line.find("\t");
			string imgName = line.substr(0,tabPos);
			int dotCount = stoi(line.substr(tabPos+1));
			//cout << imgName << "-" << dotCount << endl;
			Mat image = cv::imread("test/" + imgName + ".jpeg", CV_LOAD_IMAGE_COLOR);
			if(!image.data) {
				cout << "Error reading " << imgName << ".jpeg !" << endl;
				continue;
			}
			testImages.push_back(pair<Mat, int>(image, dotCount));
		}
		dotsFile.close();
	} else cout << "Error opening test/dots!\n";

	cout << "loaded " << testImages.size() << " images." << endl;
	return testImages.size();
}


pair<Mat, int> getTestImage(int n) {
	if(testImages.size() == 0)
		loadTestImages();

	if(testImages.size()-1 < n) {
		cout << "Image " << n << " not loaded! \n";
		return testImages[0];
	}	

	return testImages[n];
}

void runAutomatedTests() {
	Timer testTimer; testTimer.start();
	if(testImages.size() == 0)
		loadTestImages();
	int passed = 0; int n=0;
	vector<int> failed;
	for(auto img : testImages) {
		quickShow("test image", img.first);
		if(countDice(img.first, false) == img.second) {
			passed++;
			cout << "test passed !" << endl;
		} else {
			failed.push_back(n);
			cout << "test failed!" << endl;
		} 
		n++;
	}
	int total = testImages.size();
	float passed_rel = (passed/(float)total)*100;

	cout << "--------------------------\n" << "tests finished in " << testTimer << "\n--------------------------\n"
		 << "total: \t" << total		<< "\t"						<<"100%\n"
		 << "passed:\t" << passed		<< "\t" << passed_rel		<<   "%\n"
		 << "failed:\t" <<(total-passed)<< "\t" << (100-passed_rel) <<   "%\t";
	cout << "["; for(int f : failed) cout << f << " "; cout << "]\n";
	cout << "--------------------------\n";
	//waitKey(0);
	destroyAllWindows();
}

void quickShow(string name, Mat& image) {
	cv::namedWindow(name, CV_GUI_NORMAL);
	cv::imshow(name, image);
	cv::waitKey(1);
}



void patchBackground_test(Mat& input, int patchSize, int filterSize) {
	static Pipeline pipe(input);
	pipe.clear(); //in case of multiple calls

	pipe.addProcess("Patch", [](int* args, Mat& image) {
		Util::patchBackground(image, args[0], args[1]);
	}).arguments({patchSize, filterSize}).maxValue(512);

	pipe.show("patchBackground");

	input = pipe.current();
	pipe.writeImages();
}

void subtractBackground_test(Mat& input, int thr, bool useAbs) {
	static Pipeline pipe(input);
	pipe.clear();

	pipe.addProcess("sub thr", [](int* args, Mat& image) {
		static Mat background;
		static bool first = true;
		if(first) { //calc background only once
			Mat mask; getDiceMask(image, mask);
			getBackground(image, background, mask);
			first = false;
		}
		
		//Util::subtractBackground(image, background, args[0], args[1]);
		Mat result(image.size(), CV_8UC3, Scalar(0,0,0));

		for(int i = 0; i < args[0]; i+=5) {
			Mat cp = image.clone();
			Util::subtractBackground(cp, background, 100-i, args[1]);
			result += cp;
		}
		image = result;

	}).arguments({thr, useAbs}).maxValue(255);

	pipe.show("substractBackground");
	input = pipe.current();
}

void diceThreshold_test(Mat& input, Mat& output) {
	static Pipeline pipe(input);
	pipe.clear();

	pipe.addProcess("BGR2GRAY", [](int*, Mat& image) {
		cvtColor(image, image, CV_BGR2GRAY);
	}).noTrackbar();

	pipe.addProcess("Median Filter", [](int *median, Mat &image) {
	    if (*median % 2 == 0) (*median)++;
	    medianBlur(image, image, *median);
	}).arguments({5}).maxValue(25);

	pipe.addProcess("Threshold", [](int* thr, Mat& image) {
		cv::threshold(image, image,  *thr, 255, CV_THRESH_BINARY);
	}).arguments({100}).maxValue(255);

	pipe.addProcess("Median Filter 2", [](int *median, Mat &image) {
	    if (*median % 2 == 0) (*median)++;
	    medianBlur(image, image, *median);
	}).arguments({5}).maxValue(25);


	pipe.show();
	output = pipe.current();
}


//make a b/w stencil where the dice lie.
//uses circles or rectangles
void approxDiceStencil_test(Mat& input, Mat& output) {
	static Pipeline pipe(input);
	pipe.clear();
	
	pipe.addProcess("BGR2GRAY", [](int*, Mat& image) {
		cvtColor(image, image, CV_BGR2GRAY);
	}).noTrackbar();

	pipe.addProcess("Median Filter", [](int *median, Mat &image) {
	    if (*median % 2 == 0) (*median)++;
	    medianBlur(image, image, *median);
	}).arguments({5}).maxValue(25);

	pipe.addProcess("Canny", [](int* args, Mat& image) {
		Canny(image, image, args[0], args[0]*3, 3);
	}).arguments({25}).maxValue(100);

	pipe.addProcess("Boxes", [](int*, Mat& image) {
		Util::drawBoundingBoxes(image, image);
	}).noTrackbar();

	pipe.addProcess("Median after", [](int* median, Mat& image) {
		if(*median % 2 == 0) (*median)++;
		medianBlur(image, image, *median);
	}).arguments({11}).maxValue(25);

	pipe.show();
	pipe.writeImages();

	output = pipe.current();
}


void reduceNumberOfColors_test(Mat& input, Mat& output, int n) {
	static Pipeline pipe(input);
	pipe.clear();
	
	pipe.addProcess("reduce Colors", [](int* n, Mat& image) {
		Util::reduceNumberOfColors(image, image, *n);
	}).arguments({n}).maxValue(64);

	pipe.addProcess("Median", [](int* median, Mat& image) {
		if(*median % 2 == 0) (*median)++;
		medianBlur(image, image, *median);
	}).arguments({0}).maxValue(64);


	pipe.show();
	output = pipe.current();
}