/*
 * Pipeline.cpp
 *
 *  Created on: 03.11.2016
 *      Author: anton
 */
#include <highgui.h>	//inshow, namedWindow
#include <iostream>
#include "Pipeline.h"


void Pipeline::Process::on_change(int v, void* pt) {
	Process* p = (Process*) pt;
	//get a copy of the image the process is attached to
	cv::Mat copy = p->pref->at(p->pos).clone();
	p->f(&(p->args[0]), copy); //black magic		//&(vector[0]) can be used as array!
	//add modified copy to next processing step
	if (p->pos + 1 >= p->pref->pipeline.size())
		p->pref->pipeline.push_back(copy); //append as new image
	else
		p->pref->pipeline[p->pos + 1] = copy; //overwrite existing one

	//update: trigger next processing steps on changed image
	if (p->pos + 1 < p->pref->processes.size()) { //if there are subsequent steps
		Process* next = &(p->pref->processes[p->pos + 1]);
		next->on_change(next->args.size(), next); //recursive update
	} else
		p->pref->show(); //show() if this is the last step

}


void Pipeline::Process::createTrackbar(std::string winname) {
	if (hasTrackbar) { //only create if the process has a trackbar (default)
		for (int i = 0; i < args.size(); i++) { //for all possible arguments
			std::string trackbarName(std::to_string(pos + 1) + " - " + name);
			if(args.size() > 1) trackbarName += std::string(" (" + std::to_string(i + 1) + ")");
			cv::createTrackbar(trackbarName, winname, &(args[i]), max, on_change, this);
		}

		std::cout << "Added Trackbar " << name << std::endl;
	}

}


Pipeline::Pipeline(cv::Mat image) {
	pipeline.push_back(image);
}


cv::Mat& Pipeline::next() {
	std::cout << "pipeline contains " << pipeline.size() << std::endl;
	pipeline.push_back(pipeline.back().clone());

	return pipeline.back();
}


cv::Mat& Pipeline::current() {
	return pipeline.back();
}


cv::Mat& Pipeline::at(size_t pos) {
	if (pos >= pipeline.size()) {
		return pipeline.back();
	}
	return pipeline.at(pos);
}


void Pipeline::show(std::string windowName) {
	

	//init pipe, create trackbars and windows once
	if(firstCall) { firstCall = false;
		cv::namedWindow(windowName);
		if(processes.size() != 0) Process::on_change(0, &processes[0]);
		createTrackbars(windowName);
		//create a window for each pipeline step
		for (size_t i = 0; i < pipeline.size(); i++) {
			std::string pName = (i > 0 && i <= processes.size()) ? processes[i - 1].getName() : "Input";
			std::string name(std::to_string(i) + " - " + pName);
			windows.push_back(name);
			cv::namedWindow(name, CV_GUI_NORMAL);
			cv::imshow(name, pipeline.at(i));
		}

	}
	//update: show current images
	else for (size_t i = 0; i < windows.size(); i++) {
				cv::imshow(windows[i], pipeline.at(i));
			}
}


void Pipeline::createTrackbars(std::string winname) {
	for (size_t i = 0; i < processes.size(); i++) {
		processes[i].createTrackbar(winname);
	}
}

void Pipeline::writeImages() {
	for (size_t i = 0; i < pipeline.size(); i++) {
		std::string pName = (i > 0 && i <= processes.size()) ? processes[i - 1].getName() : "Image";
		std::string name(std::to_string(i) + " - " + pName);
		std::string path("./"+name+".jpeg");
		cv::imwrite(path, pipeline[i]);
	}
}
