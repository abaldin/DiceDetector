/*
 * Timer.cpp
 *
 *  Created on: 14.10.2016
 *      Author: anton
 */

#include "Timer.h"
#include <cv.h>


void Timer::start() {
	lastStop = (double)cv::getTickCount();
}

double Timer::stop() const {
	return ((double)cv::getTickCount() - lastStop) / cv::getTickFrequency();
}

double Timer::restart() {
	double time = Timer::stop();
	Timer::start();
	return time;
}

//for fancy output
std::ostream& operator<<(std:: ostream& os, const Timer& t) {
	double time = t.stop();
	return os << "[" << std::fixed << std::setprecision(3) <<time << "s]";
}



/**
 * duration of last frame
 * considers the last frame for a smoothed average
 *
 * basically timer::restart + smoothing
 */
double delta() {
	static Timer t;
	static double delta = 0;

	delta = (t.restart()*.3 + delta*.7);
	return delta;
}

/**
 * prints the fps to cout
 * - print either on change (true)
 * - or with a constant refresh rate (false, 10)
 */
void printFPS(bool onChange, int refreshRate) {
	static int counter = 0;
	static double last = 0;
	double d = delta(); //call delta in every iteration
	if( (onChange && abs(last - (1/d)) > 1) || (!onChange && counter++ >= refreshRate)) {
		last = (1 / d);
		std::cout << (int)last <<" fps"<< std::endl;
		counter = 0;
	}
}
