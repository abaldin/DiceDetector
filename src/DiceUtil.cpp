/*
 * DiceUtil.cpp
 *  
 * utility functions for dice detection
 *
 *  Created on: 14.10.2016
 *      Author: Anton Baldin
 */
#include <highgui.h>
#include <vector>
#include "DiceUtil.h"

using namespace cv;
using namespace std;


// resize an image to a desired resolution (in number of pixels)
void Util::resizeImage(Mat& image, long resolution) {
	double factor = sqrt(resolution/(double)(image.cols * image.rows));
	//if(abs(factor-1)  < 0.05) return;
	cout << "resizing image with with factor x" << factor << "\n";
	resize(image, image, Size(image.cols*factor, image.rows*factor));
}

// fills black pixels in the image with oter (non-black) pixels in the proximity.
// uses a patch vector: if a pixel is not black, it is written to the vector.
// if a black pixel is found, it will be replaced by the current pixel in the patch vector.
//
//TODO: fix possible bug where first occured pixel is black...
void Util::patchBackground(Mat& image, int patchSize, int n) {
    if(patchSize < 1) patchSize = 1;
	vector<Vec3b> patch;

    // first init the patch with non-black pixels. 
    // this is neccessary in case the upper left corner is black!
    bool initDone = false;
    for (int i = 0; i < image.rows; i++) {
		    if(initDone) break;
		    cv::Vec3b *rows = image.ptr<cv::Vec3b>(i);
            for (int j = 0; j < image.cols; j++) {
                if(rows[j] != Vec3b(0,0,0)) {
                    patch.push_back(rows[j]);
                    if(patch.size() == patchSize) {
                        initDone = true;
                        break;
                    }
                }
            }
    }

    // prevent bug
    //patchSize = patch.size();

    // now start the actual patching
	int p = 0; //patch access
    for (int i = 0; i < image.rows; i++) {
		//get the row pointer
		cv::Vec3b *rows = image.ptr<cv::Vec3b>(i);

		for (int j = 0; j < image.cols; j++) {
            //if a pixel is black (=broken)
            if(rows[j] == Vec3b(0,0,0) ) {
                //patch the current pixel
                rows[j] = patch[p];
                p = (p+1)%patchSize;
            } else {
                //update the patch
                patch[p] = rows[j];
                p = (p+1)%patchSize;
            }
		}	
    }
    //smooth image
    if(n != 0) {
        if(n % 2 == 0) ++n;
        blur(image, image, Size(n,n));
    }
}

// another approach for fixing the black holes
// idea: replace a black pixel with a non black one from the neighborhood.
// repeat until there are no black pixels left
void Util::patchBackground2(Mat& image) {
    while(hasColor(image, Vec3b(0,0,0))) {
        for (int i = 0; i < image.rows; i++) {
		    cv::Vec3b *rows = image.ptr<cv::Vec3b>(i);
		    for (int j = 0; j < image.cols; j++) {
                //TODO...maybe later (never)
            }
        }
    }
}

// returns true if at least one pixel equals the given color
bool Util::hasColor(Mat& image, Vec3b color) {
    for (int i = 0; i < image.rows; i++) {
		cv::Vec3b *rows = image.ptr<cv::Vec3b>(i);
		for (int j = 0; j < image.cols; j++)
            if(rows[j] == color) return true;
    }
    return false;
}

// fix for the reflections on dots
void Util::fillHolesInCircles(Mat& input, Mat& mask, Vec3b dotColor) {
    int radius = 12;
    for(int i = 1; i < input.rows-1; i++) {
        Vec3b *rows = input.ptr<cv::Vec3b>(i);
        for(int j = 1; j < input.cols-1; j++) {
            //only look at the masked region
            if(mask.at<u_char>(i,j) != 0) {
                int n_neighbors = getNeighborCount(input, i, j, dotColor);
                if(rows[j] != dotColor) {
                    //only fill in pixels with more than 4 correct neighbors
                    if(rows[j] != dotColor && n_neighbors >= 5) {
                        rows[j] = dotColor; continue;
                    }

                    //algorithm that checks wether a white pixel lies near black ones
                    //followed by white again (model of the dot rim)
                    int firstBlack = 0; //first black pixel occurrence
                    int dotRim = 0;     //first white pixel occurrence after a black one has occurred

                    int firstBlack_left = 0;
                    int dotRim_left = 0;
                    for(int p=j+1; p <=j+radius; p++) {
                        if(firstBlack == 0 && rows[p] == dotColor) firstBlack = p;
                        if(firstBlack != 0 && rows[p] != dotColor) dotRim = p;

                        if(dotRim_left == 0 && rows[p] != dotColor) dotRim_left = p;
                        if(dotRim_left != 0 && rows[p] == dotColor) firstBlack_left = p;
                    }
                    if((firstBlack != 0 && dotRim != 0) || (firstBlack_left != 0 && dotRim_left != 0)) {
                        //found a candidate
                        rows[j] = dotColor;
                    }   
                    



                     //remove mutations (pixels that fit not into a circle shape)
                    if(rows[j] == dotColor && n_neighbors <= 3) {
                        rows[j] = Vec3b(255,255,255) - dotColor;
                        continue;
                    }

                }
            }
            
        }
    }
}

int Util::getNeighborCount(Mat& input, int i, int j, Vec3b condColor) {
    int n_neighbors = 0;
    if(input.at<Vec3b>(i,j+1) == condColor) n_neighbors++;
    if(input.at<Vec3b>(i,j-1) == condColor) n_neighbors++;
    if(input.at<Vec3b>(i+1,j) == condColor) n_neighbors++;
    if(input.at<Vec3b>(i-1,j) == condColor) n_neighbors++;
    if(input.at<Vec3b>(i+1,j+1) == condColor) n_neighbors++;
    if(input.at<Vec3b>(i-1,j+1) == condColor) n_neighbors++;
    if(input.at<Vec3b>(i+1,j-1) == condColor) n_neighbors++;
    if(input.at<Vec3b>(i-1,j-1) == condColor) n_neighbors++;
    return n_neighbors;
}


void Util::reduceNumberOfColors(Mat& input, Mat& output, int n_colors) {
    Vec3b pixel;
    for(int i = 0; i < input.rows; i++) {
        Vec3b *rows = input.ptr<cv::Vec3b>(i);
        for(int j = 0; j < input.cols; j++) {
            pixel = rows[j];
            pixel[0] = ((pixel[0]*n_colors)/255)*(255./n_colors);
            pixel[1] = ((pixel[1]*n_colors)/255)*(255./n_colors);
            pixel[2] = ((pixel[2]*n_colors)/255)*(255./n_colors);
            output.at<Vec3b>(i, j) = pixel;    
        }
    }
}


//kmeans, taken from here:
//http://stackoverflow.com/questions/10240912/input-matrix-to-opencv-kmeans-clustering
void Util::kMeans(Mat &src, Mat &new_image, int clusterCount) {
    Mat samples(src.rows * src.cols, 3, CV_32F);
    for (int y = 0; y < src.rows; y++)
        for (int x = 0; x < src.cols; x++)
            for (int z = 0; z < 3; z++)
                samples.at<float>(y + x * src.rows, z) = src.at<Vec3b>(y, x)[z];

    //int clusterCount = 15;
    Mat labels;
    int attempts = 5;
    Mat centers;
    kmeans(samples, clusterCount, labels, TermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 10000, 0.0001), attempts, KMEANS_PP_CENTERS, centers);

    //Mat new_image( src.size(), src.type() );
    for (int y = 0; y < src.rows; y++)
        for (int x = 0; x < src.cols; x++) {
            int cluster_idx = labels.at<int>(y + x * src.rows, 0);
            new_image.at<Vec3b>(y, x)[0] = centers.at<float>(cluster_idx, 0);
            new_image.at<Vec3b>(y, x)[1] = centers.at<float>(cluster_idx, 1);
            new_image.at<Vec3b>(y, x)[2] = centers.at<float>(cluster_idx, 2);
        }
}

// compares an image with a background pixel by pixel
// the difference for each pixel is calculated and compared to a threshold. 
// if the threshold applies, the pixel in image is replaced
//
// Note: best parameters depend on dice color and shadows, see comment below
void Util::subtractBackground(Mat& image, Mat& background, int thr, bool useAbs, Vec3b color, bool inverted) {
    Vec3i diff;
    for (int i = 0; i < image.rows; i++) {
		//get the row pointer
		cv::Vec3b *rows = image.ptr<cv::Vec3b>(i);
		for (int j = 0; j < image.cols; j++) {
           // diff = rows[j] - background.at<Vec3b>(i,j);
           Vec3i p_image = rows[j]; //use Vec3i (signed integer) for abs()
           Vec3i p_backg = background.at<Vec3b>(i,j);
           diff = p_image - p_backg;

            //without abs: great way to remove shadows (dark area - bright area becomes negative 
            //and thus gets removed by the threshold). Works very well with white dice. 
            //Problem: dice with dark surface get removed too (very bad if dots are also darkish)

            //with abs: negative values are counted as big differences and therefore remain untouched.
            //this pays of for dark dice (the complete die is preserved), but does not remove shadows.
            //also the unwanted sides of the dice usually remain
            if(useAbs) 
                for(int i=0; i<3; ++i) diff[i] = abs(diff[i]);

            if(diff[0] < thr && diff[1] < thr && diff[2] < thr) {
                //rows[j] = diff;
                if(!inverted) rows[j] = color;
            } else {
                //diff is high
                if(inverted) rows[j] = color;
            }

        }
    }

}


// approximates edges to contours and draws filled bounding boxes / enclosing circles around them.
// Idea: separate the dice from the background by overpainting them with black "holes",
// which get filled with background pixels in a later step. The resulting backround image without
// the dice can be used then for subtraction (I know, this is crazy)
//
// some of the code is borrowed from here:
// http://docs.opencv.org/2.4/doc/tutorials/imgproc/shapedescriptors/bounding_rects_circles/bounding_rects_circles.html
//
//TODO: clean this mess up, half of it is not used...
void Util::drawBoundingBoxes(Mat &src_edges, Mat &output) {
    vector<vector<Point>> contours;
    vector<Vec4i> hierarchy;

    /// Find contours
    findContours(src_edges, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

    /// Approximate contours to polygons + get bounding rects and circles
    vector<vector<Point>> contours_poly(contours.size());
    vector<Rect> boundRect(contours.size());
    vector<Point2f> center(contours.size());
    vector<float> radius(contours.size());

    for (int i = 0; i < contours.size(); i++) {
        approxPolyDP(Mat(contours[i]), contours_poly[i], 3, true);
        boundRect[i] = boundingRect(Mat(contours_poly[i]));
        minEnclosingCircle((Mat)contours_poly[i], center[i], radius[i]);
    }

    /// Draw polygonal contour + buonding rects + circles
    Mat drawing = Mat::zeros(src_edges.size(), CV_8UC1);

    for (int i = 0; i < contours.size(); i++) {
        //drawContours( drawing, contours_poly, i, green, 1, 8, vector<Vec4i>(), 0, Point() );
        //rectangle( drawing, boundRect[i].tl(), boundRect[i].br(), Scalar(255,255,255), -1, 8, 0 );
        circle(drawing, center[i], (int)radius[i], Scalar(255), -1, 8, 0);
    }
    output = drawing;
}



//TODO: remove this bullshit, does not work at all
void Util::findLines(Mat &src_edges, Mat &output) {
    vector<Vec2f> lines;
    HoughLines(src_edges, lines, 4, CV_PI / 360, 100, 0, 0);

    Mat drawing = output.clone();
    cvtColor(drawing, drawing, CV_GRAY2BGR);

    //draw the lines
    for (size_t i = 0; i < lines.size(); i++) {
        float rho = lines[i][0], theta = lines[i][1];
        Point pt1, pt2;
        double a = cos(theta), b = sin(theta);
        double x0 = a * rho, y0 = b * rho;
        pt1.x = cvRound(x0 + 1000 * (-b));
        pt1.y = cvRound(y0 + 1000 * (a));
        pt2.x = cvRound(x0 - 1000 * (-b));
        pt2.y = cvRound(y0 - 1000 * (a));
        line(drawing, pt1, pt2, Scalar(0, 0, 255), 1, CV_AA);
    }

    output = drawing;

}