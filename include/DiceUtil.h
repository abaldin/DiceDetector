/*
 * DiceUtil.h
 *
 *  Created on: 26.11.2016
 *      Author: anton
 */

#ifndef SRC_HEADERS_DICEUTIL_H_
#define SRC_HEADERS_DICEUTIL_H_

#include <cv.h>

namespace Util {

void resizeImage(cv::Mat& image, long resolution);
void patchBackground(cv::Mat& image, int patchSize, int blur = 0);
void patchBackground2(cv::Mat& image);
bool hasColor(cv::Mat& image, cv::Vec3b color);
void reduceNumberOfColors(cv::Mat& input, cv::Mat& output, int n_colors);
void fillHolesInCircles(cv::Mat& input, cv::Mat& mask, cv::Vec3b dotColor = cv::Vec3b(0,0,0));
int getNeighborCount(cv::Mat& input, int i, int j, cv::Vec3b condColor);
void kMeans(cv::Mat &src, cv::Mat &new_image, int clusterCount);
void subtractBackground(cv::Mat& image, cv::Mat& background, int thr = 80, bool useAbs = false, cv::Vec3b color = cv::Vec3b(0,0,0), bool inverted = false);
void drawBoundingBoxes(cv::Mat& src_edges, cv::Mat& output);
void findLines(cv::Mat& src_edges, cv::Mat& output);

}



#endif /* SRC_HEADERS_DICEUTIL_H_ */
