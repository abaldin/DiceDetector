/*
 * CommandLine.h
 *
 *  Created on: 05.01.2017
 *      Author: anton
 */

#ifndef SRC_HEADERS_COMMANDLINE_H_
#define SRC_HEADERS_COMMANDLINE_H_

#include <string>
#include <functional>

namespace cmd {
    void addCommand(std::string cmd, std::function<void()> func);
    void init();
    void showPrompt();
}

#endif /* SRC_HEADERS_COMMANDLINE_H_ */
