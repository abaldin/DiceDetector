/*
 * Timer.h
 *
 *  Created on: 14.10.2016
 *      Author: anton
 */

#ifndef HEADERS_TIMER_H_
#define HEADERS_TIMER_H_

#include <string>

typedef struct Timer {

	double lastStop = 0;

	void start();
	double stop() const;
	double restart();

} Timer;

std::ostream& operator<<(std:: ostream& os, const Timer& t);

double delta();
void printFPS(bool onChange = true, int refreshRate = 10);

#endif /* HEADERS_TIMER_H_ */
