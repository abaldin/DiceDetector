/*
 * DiceTest.h
 *
 *  Created on: 26.11.2016
 *      Author: anton
 */

#ifndef SRC_HEADERS_DICETEST_H_
#define SRC_HEADERS_DICETEST_H_

#include <cv.h>

int loadTestImages();
std::pair<cv::Mat, int> getTestImage(int n);
void runAutomatedTests();
void quickShow(std::string name, cv::Mat& image);

void patchBackground_test(cv::Mat& input, int patchSize, int filterSize);
void subtractBackground_test(cv::Mat& input, int thr, bool useAbs=false);
void diceThreshold_test(cv::Mat& input, cv::Mat& output);
void approxDiceStencil_test(cv::Mat& input, cv::Mat& output);
void reduceNumberOfColors_test(cv::Mat& input, cv::Mat& output, int n);

#endif /* SRC_HEADERS_DICETEST_H_ */
