/*
 * DiceTest.h
 *
 *  Created on: 27.11.2016
 *      Author: anton
 */

#ifndef SRC_HEADERS_DICE_H_
#define SRC_HEADERS_DICE_H_

#include <cv.h>
void pollInput(cv::Mat& currentFrame);

void startVideoCapture();

void getDiceMask(cv::Mat &input, cv::Mat &output, int a=25, int b=75, bool extraBlur = true);
void getBackground(cv::Mat& input, cv::Mat& output, cv::Mat& mask);
void sumUpBgSubtractions(cv::Mat& image, cv::Mat& background, int thr_first, int thr_last, std::vector<cv::Mat>& results);
void getRidOfReflections(cv::Mat& image, cv::Mat& mask, int maxIterations = 64, bool do_threshold = false);
int blobDetect(cv::Mat &image, cv::Mat& mask, bool override=false);
int countDice(cv::Mat image, bool writeToDisk = false);

#endif /* SRC_HEADERS_DICE_H_ */
