/*
 * Pipeline.h
 *
 *  Created on: 03.11.2016
 *      Author: anton
 */

#ifndef SRC_HEADERS_PIPELINE_H_
#define SRC_HEADERS_PIPELINE_H_

#include <vector>


typedef struct Pipeline {

	class Process {
		typedef void (*callback)(int*, cv::Mat&);

	private:
		std::string name;
		callback f;
		Pipeline* pref;
		std::vector<int> args;

		int max = 255;				//TODO: individual max for each arg?
		int pos=-1;
		bool hasTrackbar = true;


	public:
		template<typename Func>
		Process(std::string name, Func f, Pipeline* pref, std::vector<int> args)
			:name(name), f(f), pref(pref), args(args){}

		std::string getName() {
			return name;
		}
		

		Process& noTrackbar() {
			hasTrackbar = false;
			return *this;
		}

		Process& arguments(std::vector<int> arguments) {
			args = arguments;
			return *this;
		}

		Process& maxValue(int maxValue) {
			max = maxValue;
			return *this;
		}

		//actually private functions ! 
		//TODO: make Pipeline a class and declare Process as friend.
		static void on_change(int v, void* pt);
		void createTrackbar(std::string winname);

		void setPostition(int position) {
		    pos = position;
		}

		
};


	std::vector<cv::Mat>pipeline;
	std::vector<Process>processes;

	//for show()
	bool firstCall = true;
	std::vector<std::string> windows;

	Pipeline(cv::Mat image);
	cv::Mat& next();
	cv::Mat& current();
	cv::Mat& at(size_t pos);
	void show(std::string windowName = "ControlPanel");
	void createTrackbars(std::string winname);

	template<typename Func>
	Process& addProcess(std::string name, Func f) {
		Process p(name, f, this, {0});
		p.setPostition(processes.size()); //remember position in list!
		processes.push_back(p);
		return processes.back();
	}
	void addProcess(Process p) {
		processes.push_back(p);
	}

	void clear() {
		cv::Mat tmp = pipeline[0];
		pipeline.clear();
		processes.clear();

		pipeline.push_back(tmp);
	}
	void writeImages();


}Pipeline;



#endif /* SRC_HEADERS_PIPELINE_H_ */
