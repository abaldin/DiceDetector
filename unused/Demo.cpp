/*
 * Demo.cpp
 *
 *  Created on: 10.10.2016
 *      Author: anton
 */

#include <cv.h>
#include <highgui.h>

#include "Timer.h"

using namespace cv;

bool END_LOOP_REQUEST = false;

bool GAUSSIAN = false;
bool MEDIAN = false;

int filtersize = 3;

void manipulateFrame(Mat& frame);
void pollInput();

int main( int argc, char** argv )
{
	std::string windowName = "Hello World!";
	namedWindow(windowName);

	VideoCapture capture = VideoCapture(1);
	if(!capture.isOpened()) {
		std::cerr << "ERROR: unable to open video capture!" << std::endl;
		return 1;
	}

	Mat frame;

	while(!END_LOOP_REQUEST) {
		printFPS(true);
		capture >> frame;

		pollInput();
		manipulateFrame(frame);

		//show the frame:
		imshow(windowName, frame);
	}

	return 0;
}

void manipulateFrame(Mat& frame) {
	//convert frame to gray
	cvtColor(frame, frame, CV_BGR2GRAY);
	//turn all pixels < 64 to black
	threshold(frame, frame, 64, 0, CV_THRESH_TOZERO);

	if(GAUSSIAN)
		GaussianBlur(frame.colRange(320, 640), frame.colRange(320,640), Size(filtersize, filtersize), 0);
	if(MEDIAN)
		medianBlur(frame.colRange(320,640), frame.colRange(320,640), filtersize);

	//draw a red line:
	if(GAUSSIAN || MEDIAN) {
		//convert to color again to draw a red line
		cvtColor(frame, frame, CV_GRAY2BGR);
		line(frame, Point(320,0), Point(320,480), Scalar(0,0,255), 2);
	}
}


void pollInput() {
	char keyPressed = cv::waitKey(1);

	switch(keyPressed) {
	case 27: //ESC
		END_LOOP_REQUEST = true;
		break;

	case'g':
		GAUSSIAN = !GAUSSIAN;
		std::cout << "Gaussian filter: " << GAUSSIAN << std::endl;
		break;

	case 'm':
		MEDIAN = !MEDIAN;
		std::cout << "median filter: " << MEDIAN << std::endl;
		break;

	case '+':
		filtersize = filtersize >= 99 ? 99 : filtersize +2;
		std::cout << "filter size: " << filtersize << std::endl;
		break;

	case '-':
		filtersize = filtersize <= 1 ? 1 : filtersize -2;
		std::cout << "filter size: " << filtersize << std::endl;
		break;

	default:
		break;
	}
}
